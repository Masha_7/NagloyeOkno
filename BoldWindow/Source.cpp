#include <string>
#include <cstdio>
#include <SFML/Graphics.hpp>
#include <SFML/Main.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <SFML/Audio.hpp>
#include <Windows.h>
#include <WinUser.h>
#define TIME 8.f
void SetTransparency(HWND hwnd, std::uint8_t Transperancy)
{
	long wAttr = GetWindowLong(hwnd, GWL_EXSTYLE);
	SetWindowLong(hwnd, GWL_EXSTYLE, wAttr | WS_EX_LAYERED);
	SetLayeredWindowAttributes(hwnd, 0, Transperancy, 0x02);
}
int WinMain(
	HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR     lpCmdLine,
	int       nShowCmd
)
{
	//time elapsed
	sf::Clock clock;
	sf::Time time = sf::seconds(TIME);
	while (clock.getElapsedTime() <= time);
	sf::RenderWindow window(sf::VideoMode::getFullscreenModes()[0], "",sf::Style::Fullscreen);
	SetTransparency(window.getSystemHandle(), (uint8_t)190);
	SetForegroundWindow(window.getSystemHandle());
	SetWindowPos(window.getSystemHandle(), HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
	//create text
	sf::Text text;
	sf::Font font;
	font.loadFromFile("Font.ttf");
	text.setFont(font);
	std::wstring txt = L"�� ���������� �������� 40 �����, ��������� ��� �������� ������";
	text.setString(txt);
	text.setCharacterSize(64);
	text.setFillColor(sf::Color::White);
	//center text
	sf::FloatRect textRect = text.getLocalBounds();
	text.setOrigin(textRect.left + textRect.width / 2.0f,
		textRect.top + textRect.height / 2.0f);
	text.setPosition(sf::Vector2f(window.getSize().x / 2.0f, window.getSize().y / 2.0f));
	//window.clear(sf::Color::Black);
	window.draw(text);
	window.display();
	//���������� ������
	sf::Music pi;
	pi.openFromFile("pi.wav");
	clock.restart();
	pi.play();
	time = sf::seconds(60);
	//event handeling
	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed) window.close();
		}
		if (clock.getElapsedTime() > time) window.close();
	}

	return 0;
}